import React from "react";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng
} from "react-places-autocomplete";

class LocationSearchInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { address: "" ,
    lat:null,
    lng:null,
    url:null,
    weather:null};
  }

  handleChange = address => {
    this.setState({ address });
  };

  handleSelect = address => {
 
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng =>(`http://api.openweathermap.org/data/2.5/weather?lat=${latLng.lat}&lon=${latLng.lng}&appid=e1a5f0e6dbe87d1067ada6dd9a2ee751`))
      .then(url=>this.setState({url:url}))
      .then(url=>(fetch(this.state.url)))
      .then(res=>(res.json()))
      .then(res=>this.setState({weather:res}))
      .catch(error => console.error("Error", error));
  };
 componentDidMount(){
    this.handleSelect()
  }
  render() {
     console.log(this.state.weather)
    return (
      <PlacesAutocomplete
        value={this.state.address}
        onChange={this.handleChange}
        onSelect={this.handleSelect}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <div>
            <input
              {...getInputProps({
                placeholder: "Search Places ...",
                className: "location-search-input"
              })}
            />
            <div className="autocomplete-dropdown-container">
              {loading && <div>Loading...</div>}
              {suggestions.map(suggestion => {
                const className = suggestion.active
                  ? "suggestion-item--active"
                  : "suggestion-item";
                // inline style for demonstration purpose
                const style = suggestion.active
                  ? { backgroundColor: "#fafafa", cursor: "pointer" }
                  : { backgroundColor: "#ffffff", cursor: "pointer" };
                return (
                  <div
                    {...getSuggestionItemProps(suggestion, {
                      className,
                      style
                    })}
                  >
                    <span>{suggestion.description}</span>
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
    );
  }
}

export default LocationSearchInput;
